package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.service.IImagenService;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("imagen")
@CrossOrigin({"*"})
public class ImagenControllerImpl implements IImagenController {
    private final IImagenService imagenService;
    GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
    static GenericResponseDTO GENERIC = new GenericResponseDTO();

    public ImagenControllerImpl(IImagenService imagenService) {
        this.imagenService = imagenService;
    }


    @Override
    @PostMapping("/")
    @Operation(summary = "Guardar persona")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Persona Guardada",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = PersonaDAO.class),
                                    examples = @ExampleObject(value = "{\n" +
                                            "\"personaNombre\":\"Danilo\",\n" +
                                            "\"personaApellido\":\"Arbelaez\",\n" +
                                            "\"idTipoDocumento\":1,\n" +
                                            "\"personaEdad\":14,\n" +
                                            "\"idCiudad\":23,\n" +
                                            "\"personaIdentificacion\":123456789,\n" +
                                            "\"descripcionFoto\":\"fotografia\"\n" +
                                            "}"))
                    }),
            @ApiResponse(responseCode = "400",description = "Invalido",
                    content = @Content)
    })
    public ResponseEntity<GenericResponseDTO> crearImagen(@RequestBody String json) throws ParseException, IOException {
        ImagenDAO imagenDAO= imagenService.crearImagen(json);
        return respuestaGenerica(imagenDAO);
    }

    @Override
    @GetMapping("/listar")
    public ResponseEntity<GenericResponseDTO> consultarImagenes() throws ParseException {
        List<ImagenDAO> imagenDAOS = imagenService.consultarImagenes();
        return respuestaGenerica(imagenDAOS);
    }

    @Override
    @GetMapping("/byId/{id}")
    public ResponseEntity<GenericResponseDTO> consultarImagenPorId(@PathVariable("id") String id) {
        ImagenDAO imagenDAO= imagenService.consultarImagenPorId(id);
        return respuestaGenerica(imagenDAO);
    }

    @Override
    public ResponseEntity<GenericResponseDTO> eliminarImagen(String id) {
        return null;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> actualizarImagen(String id, MultipartFile multipartFile) {
        return null;
    }

    protected ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(genericResponseDTO.getMessage())
                .objectResponse(respuestaTramite).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
    }
}
