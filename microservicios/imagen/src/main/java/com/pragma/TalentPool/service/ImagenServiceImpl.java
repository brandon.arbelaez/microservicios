package com.pragma.TalentPool.service;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.config.Util;
import com.pragma.TalentPool.repository.imagen.IImagenRepository;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.Binary;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class ImagenServiceImpl implements IImagenService{
    private final IImagenRepository imagenRepository;
    private final ModelMapper modelMapper;
    private final Util util;

    @Override
    public ImagenDAO crearImagen(String json) throws ParseException, IOException {
        ImagenDAO imagen = imagenRepository.insert(util.convertJsonToDTO(json));
        return imagen;
    }

    @Override
    public List<ImagenDAO> consultarImagenes() throws ParseException {
        List<ImagenDAO> imagenDAOS = imagenRepository.findAll();
        return imagenDAOS;
    }

    @Override
    public ImagenDAO consultarImagenPorId(String id) {
        ImagenDAO imagend = imagenRepository.buscarPorId(String.valueOf(id));
        return imagend;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> eliminarImagen(String id) {
        return null;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> actualizarImagen(String id, MultipartFile multipartFile) {
        return null;
    }
}
