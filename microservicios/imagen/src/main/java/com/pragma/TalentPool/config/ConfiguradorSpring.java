package com.pragma.TalentPool.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.pragma.TalentPool.service")
@PropertySource("classpath:application.properties")
public class ConfiguradorSpring {

}
