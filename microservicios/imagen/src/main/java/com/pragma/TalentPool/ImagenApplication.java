package com.pragma.TalentPool;

import com.pragma.TalentPool.repository.imagen.IImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ImagenApplication {

	@Autowired
	IImagenRepository imagenRepository;

	public static void main(String[] args) {
		SpringApplication.run(ImagenApplication.class, args);
	}

}

