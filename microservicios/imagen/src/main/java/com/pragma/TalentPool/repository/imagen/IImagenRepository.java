package com.pragma.TalentPool.repository.imagen;

import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface IImagenRepository extends MongoRepository<ImagenDAO,String> {

    @Query("{idPersona:'?0'}")
    ImagenDAO consultarImagenPorId(Integer id);

    @Query(value="{_id:'?0'}", fields="{'descripcionFoto' : 1,'idPersona' : 1}")
    List<ImagenDAO> findAll(String idPersona);

    @Query(value="{idPersona:'?0'}", fields="{'_id' : 1, 'descripcionFoto' : 1,'idPersona' : 1}")
    ImagenDAO buscarPorId(String idPersona);

    @Query(value = "{idPersona:'?0'}", fields="{'descripcionFoto' : 1,'idPersona' : 1}")
    ImagenDAO findByPersonaid(String idPersona);

    @Query(value = "{idPersona:'?0'}", fields="{'descripcionFoto' : 1,'idPersona' : 1}")
    List<ImagenDAO> listFindByPersonaid(String idPersona);
}
