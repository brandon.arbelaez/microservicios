package com.pragma.TalentPool.service;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IImagenService {
    ImagenDAO crearImagen(String json) throws ParseException, IOException;
    List<ImagenDAO> consultarImagenes() throws ParseException;
    ImagenDAO consultarImagenPorId(String id);
    ResponseEntity<GenericResponseDTO> eliminarImagen(@PathVariable String id);
    ResponseEntity<GenericResponseDTO> actualizarImagen(String id, MultipartFile multipartFile);
}
