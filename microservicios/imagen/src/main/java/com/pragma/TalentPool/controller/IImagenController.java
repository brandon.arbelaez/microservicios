package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IImagenController {
    ResponseEntity<GenericResponseDTO> crearImagen(String json) throws ParseException, IOException;
    ResponseEntity<GenericResponseDTO> consultarImagenes() throws ParseException;
    ResponseEntity<GenericResponseDTO> consultarImagenPorId(String id);
    ResponseEntity<GenericResponseDTO> eliminarImagen(@PathVariable String id);
    ResponseEntity<GenericResponseDTO> actualizarImagen(String id, MultipartFile multipartFile);

}
