package com.pragma.TalentPool.config;

import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import lombok.AllArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@AllArgsConstructor
@Component
public class Util {

    private final ModelMapper modelMapper;
    static PersonaDTO personaDTO;
    static ImagenDAO imagenDAO;
    public ImagenDAO convertJsonToDTO(String json) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject parseJson = (JSONObject) parser.parse(json);
        JSONObject imagen = (JSONObject) parseJson.get("imagen");
        Object idPersona = parseJson.get("idPersona");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(imagen);
        jsonArray.parallelStream().forEach(temporal -> {
            JSONObject temp = (JSONObject) temporal;
            imagenDAO= modelMapper.map(temp, ImagenDAO.class);
            imagenDAO.setIdPersona(String.valueOf(idPersona));
        });

        return imagenDAO;
    }

    public PersonaDTO convertJsonImg(String json, ImagenDAO imagenDAO) throws ParseException {

        JSONParser parser = new JSONParser();
        JSONObject parseJson = (JSONObject) parser.parse(json);
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(parseJson);
        jsonArray.parallelStream().forEach(temporal -> {
            JSONObject temp = (JSONObject) temporal;
            personaDTO= modelMapper.map(temp, PersonaDTO.class);
            modelMapper.map(imagenDAO,personaDTO);
        });
        return personaDTO;
    }

    public List<PersonaDTO> personaDAOtoDTO(List<PersonaDAO> personaDAO, ModelMapper modelMapper) {
        List<PersonaDTO> personaDTO = new ArrayList<>();
        modelMapper.map(personaDAO, personaDTO);
        return personaDTO;

    }

    public Binary fileToBytes(MultipartFile file) throws IOException {
        return new Binary(BsonBinarySubType.BINARY, file.getBytes());
    }

}
