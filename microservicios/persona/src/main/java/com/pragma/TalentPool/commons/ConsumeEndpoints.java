package com.pragma.TalentPool.commons;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import lombok.AllArgsConstructor;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@Component
public class ConsumeEndpoints {
    private final RestTemplate template;
    private final ModelMapper modelMapper;
    static PersonaDTO personaDTO;
    static ImagenDAO imagenDAO;
    static String URL = "http://servicio-imagen/imagen";

    public ResponseEntity<GenericResponseDTO> guardarImagen(JSONObject json) {
        ResponseEntity<GenericResponseDTO> imagenDAOResponseEntity = template.postForEntity(URL + "/", json, GenericResponseDTO.class);
        return imagenDAOResponseEntity;
    }

    public ResponseEntity<GenericResponseDTO> consultarImagenPorId(String id) {

        ResponseEntity<GenericResponseDTO> forObject = template.getForEntity(URL + "/byId/{p1}", GenericResponseDTO.class,id);
        return forObject;
    }
}
