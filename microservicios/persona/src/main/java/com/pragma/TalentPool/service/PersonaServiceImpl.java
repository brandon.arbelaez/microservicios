package com.pragma.TalentPool.service;

import com.pragma.TalentPool.commons.ConsumeEndpoints;
import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.commons.Util;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericNullPointerException;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericRunTimeException;
import com.pragma.TalentPool.repository.persona.IPersonaRepository;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class PersonaServiceImpl implements IPersonaService {
    private final IPersonaRepository repository;
    private final ModelMapper modelMapper;
    private final Util util;
    private final ConsumeEndpoints consumeEndpoints;

    static PersonaDTO respuestaTramite = new PersonaDTO();
    static String MENSAJE = "";
    static HttpStatus STATUS_OK = HttpStatus.OK;
    static GenericResponseDTO GENERIC = new GenericResponseDTO();
    static String tipId;

    @Override
    public ResponseEntity<GenericResponseDTO> crearPersona(String json) throws ParseException, IOException {
        ImagenDAO imagenDAO = new ImagenDAO();
        PersonaDAO personaDAO = new PersonaDAO();
        respuestaTramite = util.convertJsonToDTO(json);
        if (repository.buscar(respuestaTramite.getPersonaIdentificacion()) == null) {
            modelMapper.map(respuestaTramite, personaDAO);
            PersonaDAO persona = repository.save(personaDAO);
            respuestaTramite.setIdPersona(persona.getIdPersona());
            JSONObject jsonObject = util.convertDTOTojson(respuestaTramite);
            ResponseEntity<GenericResponseDTO> imagen = consumeEndpoints.guardarImagen(jsonObject);
            Object objectResponse = imagen.getBody().getObjectResponse();
            modelMapper.map(persona, respuestaTramite);
            modelMapper.map(objectResponse,imagenDAO);
            modelMapper.map(imagenDAO, respuestaTramite);
            MENSAJE = "Se guarda la pesona con la imagen: ";
        } else {
            throw new GenericRunTimeException("Ya existe la persona con ese numero de documento ", 400);
        }
        return respuestaGenerica(respuestaTramite, MENSAJE, GENERIC.getStatusCode(), STATUS_OK);
    }

    @Override
    public ResponseEntity<GenericResponseDTO> actualizarPersona(String json, MultipartFile file) throws ParseException {
        ImagenDAO imagenDAO = new ImagenDAO();
        PersonaDAO personaDAO = new PersonaDAO();
        respuestaTramite = util.convertJsonToDTO(json);
        Optional<PersonaDAO> buscarPorId = repository.findById(respuestaTramite.getIdPersona());
        if (buscarPorId.isPresent()) {
            modelMapper.map(respuestaTramite, personaDAO);
            repository.save(personaDAO);
            MENSAJE = "Se actualiza la pesona con la imagen: ";
        } else {
            throw new GenericNullPointerException("No existe la persona con la id: ", 400);
        }
        return respuestaGenerica(respuestaTramite, MENSAJE, GENERIC.getStatusCode(), STATUS_OK);
    }

    @Override
    public List<PersonaDTO> consultarPersonas() {
        List<PersonaDTO> respuestaTramite = repository.findAll().stream().map(p -> {
            PersonaDTO personaDTO1 = new PersonaDTO(p);
            ImagenDAO imagenDAO = new ImagenDAO();
            ResponseEntity<GenericResponseDTO> response = consumeEndpoints.consultarImagenPorId(String.valueOf(p.getIdPersona()));
            Object objectResponse = response.getBody().getObjectResponse();
            modelMapper.map(objectResponse,imagenDAO);
            personaDTO1.setImagen(imagenDAO);
            return personaDTO1;
        }).collect(Collectors.toList());
        GENERIC.setMessage("Se consultan todas las personas");
        GENERIC.setStatusCode(HttpStatus.OK.value());
        return respuestaTramite;
    }

    @Override
    public List<PersonaDTO> consultarPersonaPorId(Integer id) {
        List<PersonaDTO> listaPersona = repository.consultarPersonaPorId(id).stream().map(x -> {
            PersonaDTO personaDTO = new PersonaDTO(x);
            ImagenDAO imagenDAO = new ImagenDAO();
            ResponseEntity<GenericResponseDTO> response = consumeEndpoints.consultarImagenPorId(String.valueOf(x.getIdPersona()));
            Object objectResponse = response.getBody().getObjectResponse();
            modelMapper.map(objectResponse,imagenDAO);
            personaDTO.setImagen(imagenDAO);
            return personaDTO;
        }).collect(Collectors.toList());
        if (listaPersona.size() != 0) {
            GENERIC.setMessage("Se consulta por el id " + id);
        } else {
            throw new GenericRunTimeException("No existe la persona con la id: " + id, 400);
        }
        return listaPersona;
    }

    @Override
    public List<PersonaDTO> consultarPersonaPorEdad(Integer tipoid, Integer rangoEdadInicial, Integer rangoEdadFinal) {
        List<PersonaDTO> listaPersona;
        String mensaje;
        if (tipoid == 0 && rangoEdadInicial != 0 && rangoEdadFinal != 0) {
            listaPersona = repository.consultarPorEdad(rangoEdadInicial, rangoEdadFinal).stream().map(x -> {
                PersonaDTO personaDTO = new PersonaDTO(x);
                //ImagenDAO imagend = imagenRepository.buscarPorId(String.valueOf(x.getIdPersona()));
               // personaDTO.setImagen(imagend);
                return personaDTO;
            }).collect(Collectors.toList());
            mensaje = ("Se consulta las personas por rango de edad inicial: " + rangoEdadInicial + " Y rango de edad Final: " + rangoEdadFinal);
        } else {
            listaPersona = repository.consultarPersonaPorTipoId(tipoid).stream().map(x -> {
                PersonaDTO personaDTO = new PersonaDTO(x);
                //ImagenDAO imagend = imagenRepository.buscarPorId(String.valueOf(x.getIdPersona()));
                //personaDTO.setImagen(imagend);
                tipId = personaDTO.getTipoDocumento().getTipoDocumentoDescripcion();
                return personaDTO;
            }).collect(Collectors.toList());
            mensaje = ("Se consulta las personas por tipo de identificacion " + tipId);
        }
        if (listaPersona.size() != 0) {
            GENERIC.setMessage(mensaje);
        } else {
            throw new GenericRunTimeException("No se encontraron personas: ", 400);
        }
        return listaPersona;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> eliminarPersona(Integer id) {
        Integer respuestaTramite = repository.eliminarPersona(id);
        if (respuestaTramite != 0) {
            //ImagenDAO imagenDAO = imagenRepository.findByPersonaid(String.valueOf(id));
            //imagenRepository.deleteById(imagenDAO.get_id());
            MENSAJE = "Se elimina la persona con el id: " + id;
        } else {
            throw new GenericRunTimeException("No existe la persona con la id: " + id, 400);
        }
        return respuestaGenerica(respuestaTramite, MENSAJE, GENERIC.getStatusCode(), STATUS_OK);
    }


    public ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite, String mensaje, Integer status, HttpStatus stat) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(mensaje)
                .objectResponse(respuestaTramite).statusCode(status).build(), stat);
    }
}
