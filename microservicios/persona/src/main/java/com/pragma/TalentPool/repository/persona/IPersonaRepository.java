package com.pragma.TalentPool.repository.persona;

import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface IPersonaRepository extends JpaRepository<PersonaDAO, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM PersonaDAO AS p WHERE p.id=:id")
    Integer eliminarPersona(@Param("id")Integer id);

    @Query("SELECT p FROM PersonaDAO AS p INNER JOIN TipoDocumentoDAO td ON p.tipoDocumento=td.idTipoDocumento WHERE p.personaIdentificacion=:id")
    List<PersonaDTO> consultarPersonaPorId(@Param("id")Integer id);

    @Query("SELECT p FROM PersonaDAO AS p INNER JOIN TipoDocumentoDAO td ON p.tipoDocumento=td.idTipoDocumento WHERE td.idTipoDocumento=?1")
    List<PersonaDTO> consultarPersonaPorTipoId(Integer tipoId);

    @Query("SELECT p FROM PersonaDAO AS p INNER JOIN TipoDocumentoDAO td ON p.tipoDocumento=td.idTipoDocumento WHERE p.personaEdad BETWEEN ?1 AND ?2")
    List<PersonaDTO> consultarPorEdad(Integer rangoEdadInicial, Integer rangoEdadFinal);

    @Query("SELECT p FROM PersonaDAO p WHERE p.personaIdentificacion=:id")
    PersonaDAO buscar(@Param("id") Integer id);

}
