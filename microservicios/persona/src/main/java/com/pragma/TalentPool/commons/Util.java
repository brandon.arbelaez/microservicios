package com.pragma.TalentPool.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.talentpool.entity.imagen.DAO.ImagenDAO;
import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import lombok.AllArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@Component
public class Util {
    private final RestTemplate template;
    private final ModelMapper modelMapper;
    static PersonaDTO personaDTO;
    static ImagenDAO imagenDAO;
    static String URL = "http://localhost:8081/imagen";

    public PersonaDTO convertJsonToDTO(String json) throws ParseException {

        JSONParser parser = new JSONParser();
        JSONObject parseJson = (JSONObject) parser.parse(json);
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(parseJson);
        jsonArray.parallelStream().forEach(temporal -> {
            JSONObject temp = (JSONObject) temporal;
            personaDTO=modelMapper.map(temp, PersonaDTO.class);
            imagenDAO=modelMapper.map(temp,ImagenDAO.class);
        });
        modelMapper.map(imagenDAO,personaDTO);
        return personaDTO;
    }

    public JSONObject convertDTOTojson(PersonaDTO personaDTO) throws ParseException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JSONParser parser = new JSONParser();
        JSONObject parseJson = (JSONObject) parser.parse(mapper.writeValueAsString(personaDTO));
        return parseJson;
    }

    public List<PersonaDTO> personaDAOtoDTO(List<PersonaDAO> personaDAO, ModelMapper modelMapper) {
        List<PersonaDTO> personaDTO = new ArrayList<>();
        modelMapper.map(personaDAO, personaDTO);
        return personaDTO;

    }

    public Binary fileToBytes(MultipartFile file) throws IOException {
        return new Binary(BsonBinarySubType.BINARY, file.getBytes());
    }

    public List<ImagenDAO> listObjectToDAO(List<Object> list){

        return null;
    }

}
