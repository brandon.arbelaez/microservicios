package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IPersonaController {
    ResponseEntity<GenericResponseDTO> crearPersona(String json) throws ParseException, IOException;
    ResponseEntity<GenericResponseDTO> actualizarPersona(String json, MultipartFile file) throws ParseException;
    ResponseEntity<GenericResponseDTO> consultarPersonas();
    ResponseEntity<GenericResponseDTO> consultarPersonaPorId(@PathVariable Integer id);
    ResponseEntity<GenericResponseDTO> consultarPersonaPorEdad(Integer tipoid, Integer rangoEdadInicial, Integer rangoEdadFinal);
    ResponseEntity<GenericResponseDTO> eliminarPersona(@PathVariable Integer id);
}
