package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.service.IPersonaService;
import com.pragma.talentpool.entity.persona.DAO.PersonaDAO;
import com.pragma.talentpool.entity.persona.DTO.PersonaDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("persona")
@CrossOrigin({"*"})
public class PersonaControllerImpl implements IPersonaController {
    private final IPersonaService iPersonaService;
    static GenericResponseDTO GENERIC = new GenericResponseDTO();

    public PersonaControllerImpl(IPersonaService iPersonaService) {
        this.iPersonaService = iPersonaService;
    }

    @Override
    @PostMapping("/")
    @Operation(summary = "Guardar persona")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Persona Guardada",
            content = {
                    @Content(mediaType = "application/json",
                    schema = @Schema(implementation = PersonaDAO.class),
                    examples = @ExampleObject(value = "{\n" +
                            "\"personaNombre\":\"Danilo\",\n" +
                            "\"personaApellido\":\"Arbelaez\",\n" +
                            "\"idTipoDocumento\":1,\n" +
                            "\"personaEdad\":14,\n" +
                            "\"idCiudad\":23,\n" +
                            "\"personaIdentificacion\":123456789,\n" +
                            "\"descripcionFoto\":\"fotografia\"\n" +
                            "}"))
            }),
            @ApiResponse(responseCode = "400",description = "Invalido",
            content = @Content)
    })
    public ResponseEntity<GenericResponseDTO> crearPersona( @RequestBody String json) throws ParseException, IOException {
        return iPersonaService.crearPersona(json);
    }

    @Override
    @PutMapping("/actualizar")
    public ResponseEntity<GenericResponseDTO> actualizarPersona( @RequestParam("datos") String json, @RequestParam("image") MultipartFile file) throws ParseException {
        return iPersonaService.actualizarPersona(json, file);
    }

    @Override
    @GetMapping("/consultarPersonas")
    public ResponseEntity<GenericResponseDTO> consultarPersonas() {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonas();
        return respuestaGenerica(respuestaTramite);


    }

    @Override
    @GetMapping("/consultarPersona/{id}")
    public ResponseEntity<GenericResponseDTO> consultarPersonaPorId( @PathVariable("id") Integer id) {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonaPorId(id);
        return respuestaGenerica(respuestaTramite);
    }

    @Override
    @GetMapping("/consultarPersonaPorEdad/{tipoId}/{edadIni}/{edadFin}")
    public ResponseEntity<GenericResponseDTO> consultarPersonaPorEdad( @PathVariable("tipoId")Integer tipoid, @PathVariable("edadIni") Integer rangoEdadInicial, @PathVariable("edadFin") Integer rangoEdadFinal) {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonaPorEdad(tipoid,rangoEdadInicial,rangoEdadFinal);
        System.out.println(GenericResponseDTO.builder().build().getMessage());
        return respuestaGenerica(respuestaTramite);
    }

    @Override
    @DeleteMapping("/eliminarPersona/{id}")
    public ResponseEntity<GenericResponseDTO> eliminarPersona( @PathVariable("id") Integer id) {
        return iPersonaService.eliminarPersona(id);
    }

    public ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(GenericResponseDTO.builder().build().getMessage())
                .objectResponse(respuestaTramite).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
    }
}
