package com.pragma.talentpool.entity.imagen.DAO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@Document(value = "IMAGEN")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "parent"})
public class ImagenDAO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String _id;

    private String idPersona;

    private String descripcionFoto;

    //private String descripcionFoto;
}
