package com.pragma.talentpool.entity.ciudad.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CiudadDTO {
    private Integer idCiudad;
    private String ciudadDescripcion;
    private Integer ciudadIdDepartamento;
}
