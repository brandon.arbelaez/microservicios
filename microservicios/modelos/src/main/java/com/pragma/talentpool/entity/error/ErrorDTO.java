package com.pragma.talentpool.entity.error;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDTO {
    private Integer statusCode;
    private String mensaje;
}
