package com.pragma.talentpool.entity.tipodocumento.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TipoDocumentoDTO {
    private String idTipoDocumento;
    private String tipoDocumentoDescripcion;
    private String tipoDocumentoSigla;
}
