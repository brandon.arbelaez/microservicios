package com.pragma.talentpool.entity.persona.DAO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.pragma.talentpool.entity.ciudad.DAO.CiudadDAO;
import com.pragma.talentpool.entity.tipodocumento.DAO.TipoDocumentoDAO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "personas")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "parent"})
public class PersonaDAO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "persona_id", unique = true)
    private Integer idPersona;

    @Column(name = "persona_nombre")
    private String personaNombre;

    @Column(name = "persona_apellido")
    private String personaApellido;

    @OneToOne(optional = false, cascade = {
            CascadeType.REFRESH, CascadeType.DETACH
    })
    @JoinColumn(name = "id_tipoidentificacion", nullable = false, referencedColumnName = "tipodocumento_id", foreignKey = @ForeignKey(
            name = "tipodocumento_id"
    ))
    private TipoDocumentoDAO tipoDocumento;

    @Column(name = "persona_edad")
    private Integer personaEdad;

    @OneToOne(optional = false, cascade = {
            CascadeType.REFRESH, CascadeType.DETACH
    })
    @JoinColumn(name = "id_ciudad", nullable = false, referencedColumnName = "ciudad_id", foreignKey = @ForeignKey(
            name = "fk_persona_ciudad"
    ))
    private CiudadDAO ciudad;

    @Column(name = "persona_identificacion")
    private Integer personaIdentificacion;
}
